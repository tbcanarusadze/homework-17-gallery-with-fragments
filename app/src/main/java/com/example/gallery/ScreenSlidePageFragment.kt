package com.example.gallery

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_screen_slide_page.view.*

class ScreenSlidePageFragment(var image: String, var position: Int) : Fragment() {

    lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_screen_slide_page, container, false)
        setImage()
        return itemView
    }

    @SuppressLint("SetTextI18n")
    private fun setImage() {
        Glide.with(this).load(image).into(itemView.imageView)
        itemView.textView.text = "Position: ${position + 1}"
    }


}